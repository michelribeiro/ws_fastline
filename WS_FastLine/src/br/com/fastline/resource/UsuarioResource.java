package br.com.fastline.resource;

import java.util.ArrayList;

//import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
//import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import br.com.fastline.usuarios.Usuario;
import br.com.fastline.usuarios.UsuarioBusiness;
import com.google.gson.Gson;

@Path("/usuario")
public class UsuarioResource {
	@GET
	@Path("/listarTodos")
	@Produces("application/json")

	public ArrayList<Usuario> listarTodos(){
		return new UsuarioBusiness().listarTodos();
	}

	@GET
	@Path("/listarTodosGSON")
	@Produces("application/json")
	public String selTodosGSON(){
		return new Gson().toJson(new UsuarioBusiness().listarTodos());
	}

	@GET
	@Path("/inserir/{nm_usuario}/{senha}/{nivel}/{status}")
	@Produces("application/json")
//	@Produces("text/plain")
	//	@Consumes("application/gson")
	public String inserirCliente(@PathParam("nm_usuario") String nm_usuario,@PathParam("senha") String senha,@PathParam("nivel") int nivel,@PathParam("status") int status) {
		return new UsuarioBusiness().inserir(nm_usuario,senha,nivel,status);
	}
}
