package br.com.fastline.resource;

import java.util.ArrayList;

import org.codehaus.jettison.json.JSONObject;

import br.com.fastline.teste.Teste;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class TestePostLista {
//	public static void main(String[] args) throws ClientProtocolException, IOException {
//        ClientConfig config = new DefaultClientConfig();
//        Client client = Client.create(config);
//        WebResource webResource = client.resource(UriBuilder.fromUri("http://localhost:8080/WS_FastLine").build());
//        MultivaluedMap formData = new MultivaluedMapImpl();
//        formData.add("id_teste", "1");
//        ClientResponse response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);
//        System.out.println("Response " + response.getEntity(String.class));
//    }
	
	public static void main(String[] args) {
		try {
			Teste t1 = new Teste(123);
			ArrayList<JSONObject> listaTestes = new ArrayList<JSONObject>();
			listaTestes.add(new JSONObject(t1));
			
			listaTestes.add(new Teste(9999));
			
			Gson gson = new Gson();
			String input = (gson.toJson(listaTestes)); 
//			ClientConfig clientConfig = new DefaultClientConfig();
//		    clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
			
			Client client = Client.create();
	 
			WebResource webResource = client.resource("http://localhost:8080/WS_FastLine/teste/inserirLista");
			
			
//			Teste teste = new Teste();
//			teste.setId_teste(12345);
//			Gson gson = new Gson();
//			String input = (gson.toJson(teste));
//			String input = "{\"id_teste\":\"123\"}";
			
//			ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, input );

			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, input);
	 
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
	 
			System.out.println("Output from Server .... \n");
			String output = response.getEntity(String.class);
			System.out.println(output);
	 
		  } catch (Exception e) {
	 
			e.printStackTrace();
	 
		  }
	 
		}

}
