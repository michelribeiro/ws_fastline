package br.com.fastline.resource;


import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import com.google.gson.Gson;
import br.com.fastline.produtos.Produto;
import br.com.fastline.produtos.ProdutoBusiness;


@Path("/produto")
public class ProdutoResource {
	@GET
	@Path("/listarTodos")
	@Produces("application/json")

	public ArrayList<Produto> listarTodos(){
		return new ProdutoBusiness().listarTodos();
	}

	@GET
	@Path("/listarTodosGSON")
	@Produces("application/json")
	public String selTodosGSON(){
		return new Gson().toJson(new ProdutoBusiness().listarTodos());
	}

	@POST
	@Path("/inserir")
	@Produces("application/json")
	@Consumes("application/json")
	public String inserirCliente(Produto produto) {
		return new ProdutoBusiness().inserir(produto);
	}

}
