package br.com.fastline.resource;


import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import br.com.fastline.teste.Teste;
import br.com.fastline.teste.TesteBusiness;


@Path("/teste")
public class TesteResource {
	@GET
	@Path("/listarTodos")
	@Produces("application/json")
	public ArrayList<Teste> listarTodos(){
		return new TesteBusiness().listarTodos();
	}

	@GET
	@Path("/listarTodosGSON")
	@Produces("application/json")
	public String selTodosGSON(){
		return new Gson().toJson(new TesteBusiness().listarTodos());
	}
	
//	@GET
//    @Path("/inserir/{jsonString}")
//	@Produces(MediaType.TEXT_PLAIN)
//    public String inserirTeste(@PathParam("jsonString") String jsonString) {
////		String jsonString = "{'id_teste : 1'}";
//		Gson gson = new Gson();
//		Teste teste = gson.fromJson(jsonString, Teste.class);  
//     return new TesteBusiness().inserir(teste);
//    }

	@POST
    @Path("/inserir")
//	@Produces("application/json")
	@Consumes("application/json")
    public String inserirTeste(Teste teste) {
//		String jsonString = "{'id_teste : 1'}";
//		Gson gson = new Gson();
//		Teste teste = gson.fromJson(jsonString, Teste.class);  
     return new TesteBusiness().inserir(teste);
    }

	@POST
    @Path("/inserirLista")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response inserirLista(ArrayList<Teste> listaTestes) {
//     listaTestesJson = "[{'id_teste':'22221'},{'id_teste':'22222222'}]" ;
//     Gson gson = new Gson();
//     ArrayList<Teste> listaTestes = new ArrayList<Teste>();
//     JsonParser parser = new JsonParser();
//        JsonArray array = parser.parse(listaTestesJson).getAsJsonArray();
//     
//        for (int i = 0; i < array.size(); i++) {
//         listaTestes.add(gson.fromJson(array.get(i), Teste.class));
//     }
     
//     return new TesteBusiness().inserirLista(listaTestes);
		String output = "consumeJSONList Client : " + listaTestes.toString() + "\n\n";

        return Response.status(200).entity(output).build();
    }
	
//	@POST
//    @Path("/inserirLista")
//    @Produces("application/json")
//    @Consumes("application/json")
//    public String inserirLista(String listaTestesJson) {
//     
//     Gson gson = new Gson();
//     ArrayList<Teste> listaTestes = new ArrayList<Teste>();
//     JsonParser parser = new JsonParser();
//        JsonArray array = parser.parse(listaTestesJson).getAsJsonArray();
//     
//        for (int i = 0; i < array.size(); i++) {
//         listaTestes.add(gson.fromJson(array.get(i), Teste.class));
//     }
//     
//     return new TesteBusiness().inserirLista(listaTestes);
//
//    }
	
	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createTrackInJSON(Teste teste) {
 
		String result = "Track saved : " + teste;
		return Response.status(201).entity(result).build();
 
	}


}
