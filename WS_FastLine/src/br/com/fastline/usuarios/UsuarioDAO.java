package br.com.fastline.usuarios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.fastline.factory.ConnectionFactory;

public class UsuarioDAO extends ConnectionFactory{
	public ArrayList<Usuario> listarTodos(){

		Connection conn = null;
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		ArrayList<Usuario> usuarios = null;

		conn = getConnection();
		usuarios = new ArrayList<Usuario>();

		try {

			stmt = conn.prepareStatement("SELECT * FROM tb_usuarios");
			resultSet = stmt.executeQuery();

			while (resultSet.next()) {
				Usuario usuario = new Usuario();
				usuario.setId_usuario(resultSet.getInt("id_usuario"));
				usuario.setNm_usuario(resultSet.getString("nm_usuario"));
				usuario.setSenha(resultSet.getString("senha"));
				usuario.setNivel(resultSet.getInt("nivel"));
				usuario.setStatus(resultSet.getInt("status"));

				usuarios.add(usuario);
			}

		} catch (SQLException e) {
			System.out.println("Erro ao listar todos os usuarios: " + e);
			e.printStackTrace();
			//	         e.printStackTrace();
			//	         listaProdutos = null;
		} finally {
			closeConnection(conn, stmt, resultSet);
		}
		return usuarios;
	}
	
	public String inserir(String nm_usuario,String senha,int nivel,int status){

		Connection conn = null;
		ResultSet resultSet = null;
		PreparedStatement stmt = null;

		conn = getConnection();
		
		String resultado = null;
		try {

			stmt = conn.prepareStatement("SELECT fn_crud_usuarios(NULL,?,?,?,?,'INSERT')");
			stmt.setString(1, nm_usuario);
			stmt.setString(2, senha);
			stmt.setInt(3, nivel);
			stmt.setInt(4, status);
			resultSet = stmt.executeQuery();

			while (resultSet.next()) {
////				Usuario usuario = new Usuario();
////				usuario.setId_usuario(resultSet.getInt("id_usuario"));
////				usuario.setNm_usuario(resultSet.getString("nm_usuario"));
////				usuario.setSenha(resultSet.getString("senha"));
////				usuario.setNivel(resultSet.getInt("nivel"));
////				usuario.setStatus(resultSet.getInt("status"));
////
				resultado = resultSet.getString(1);
			}

		} catch (SQLException e) {
			System.out.println("Erro ao listar todos os usuarios: " + e);
			resultado = "Falha na Insert";
			e.printStackTrace();
			//	         e.printStackTrace();
			//	         listaProdutos = null;
		} finally {
			closeConnection(conn, stmt, resultSet);
		}
		return resultado;
	}
	
//	public int inserir(Usuario usuario) {
//
//	     Connection conn = null;
//	     conn = getConnection();
//	     int sucesso = 0;
//
//	         PreparedStatement stmt = null;
//	         try {
//	             stmt = conn.prepareStatement("INSERT INTO tb_usuarios (id_usuario,nm_usuario,senha,nivel,status) VALUES(?,?,?,?,?)");
//	             stmt.setInt(1, usuario.getId_usuario());
//	             stmt.setString(2, usuario.getNm_usuario());
//	             stmt.setString(3, usuario.getSenha());
//	             stmt.setInt(4, usuario.getNivel());
//	             stmt.setInt(5, usuario.getStatus());
//	             
//	             sucesso = stmt.executeUpdate();
//
//	             if (sucesso > 0) {
//	                 System.out.println("USUARIO INSERIDO!");
//	             }
//
//	         } catch (SQLException e) {
//	             e.printStackTrace();
//	             System.out.println("ERRO AO INSERIR USUARIO!");
//	         } finally {
//	             closeConnection(conn, stmt);
//	         }
//	     return sucesso;
//	    }
}
