package br.com.fastline.usuarios;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public final class Usuario {

	private Integer id_usuario;
	private String nm_usuario;
	private String senha;
	private Integer nivel;
	private Integer status;
	public Integer getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getNm_usuario() {
		return nm_usuario;
	}
	public void setNm_usuario(String nm_usuario) {
		this.nm_usuario = nm_usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Integer getNivel() {
		return nivel;
	}
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Usuario [id_usuario=" + id_usuario + ", nm_usuario="
				+ nm_usuario + ", senha=" + senha + ", nivel=" + nivel
				+ ", status=" + status + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((id_usuario == null) ? 0 : id_usuario.hashCode());
		result = prime * result + ((nivel == null) ? 0 : nivel.hashCode());
		result = prime * result
				+ ((nm_usuario == null) ? 0 : nm_usuario.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id_usuario == null) {
			if (other.id_usuario != null)
				return false;
		} else if (!id_usuario.equals(other.id_usuario))
			return false;
		if (nivel == null) {
			if (other.nivel != null)
				return false;
		} else if (!nivel.equals(other.nivel))
			return false;
		if (nm_usuario == null) {
			if (other.nm_usuario != null)
				return false;
		} else if (!nm_usuario.equals(other.nm_usuario))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	public Usuario(Integer id_usuario, String nm_usuario, String senha,
			Integer nivel, Integer status) {
		super();
		this.id_usuario = id_usuario;
		this.nm_usuario = nm_usuario;
		this.senha = senha;
		this.nivel = nivel;
		this.status = status;
	}
	public Usuario() {
		super();
		this.id_usuario = null;
		this.nm_usuario = null;
		this.senha = null;
		this.nivel = null;
		this.status = null;
	}
	
	
	
	
}
