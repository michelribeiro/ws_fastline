package br.com.fastline.teste;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.fastline.factory.ConnectionFactory;

public class TesteDAO extends ConnectionFactory {
	public ArrayList<Teste> listarTodos(){

		Connection conn = null;
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		ArrayList<Teste> testes = null;

		conn = getConnection();
		testes = new ArrayList<Teste>();

		try {

			stmt = conn.prepareStatement("SELECT * FROM tb_teste");
			resultSet = stmt.executeQuery();

			while (resultSet.next()) {
				Teste teste = new Teste();
				teste.setId_teste(resultSet.getInt("id_teste"));
				testes.add(teste);
			}

		} catch (SQLException e) {
			System.out.println("Erro ao listar todos os clientes: " + e);
			e.printStackTrace();
			//	         e.printStackTrace();
			//	         listaProdutos = null;
		} finally {
			closeConnection(conn, stmt, resultSet);
		}
		return testes;
	}

	
	
	public int inserir(Teste teste) {

		Connection conn = null;
		conn = getConnection();
		int sucesso = 0;

		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("INSERT INTO tb_teste (id_teste) VALUES(?)");

			stmt.setInt(1, teste.getId_teste());
			sucesso = stmt.executeUpdate();

			if (sucesso > 0) {
				System.out.println("CLIENTE INSERIDO!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("ERRO AO INSERIR CLIENTE!");
		} finally {
			closeConnection(conn, stmt);
		}
		return sucesso;

	}
}