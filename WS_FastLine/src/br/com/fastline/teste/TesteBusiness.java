package br.com.fastline.teste;

import java.util.ArrayList;


public class TesteBusiness {
	public ArrayList<Teste> listarTodos() {
		TesteDAO testeDAO = new TesteDAO();
		return testeDAO.listarTodos();
	}
	
	public String inserir(Teste teste) {

		TesteDAO testeDAO = new TesteDAO();
		if(testeDAO.inserir(teste) > 0){
			return "Cliente inserido no banco com sucesso!";
		} else {
			return "Falha ao inserir o cliente no banco!";
		}
	}
	
	public String inserirLista(ArrayList<Teste> listaTestes) {
	     TesteDAO testeDAO = new TesteDAO();
	     String retorno = "";
	     for (int i = 0; i < listaTestes.size(); i++) {
	         if(testeDAO.inserir(listaTestes.get(i)) < 1){
	             retorno += "Erro ao inserir o cliente de CPF: "+ listaTestes.get(i).getId_teste()+"\n";
	         }
	     }
	     if(retorno.length() == 0){
	         retorno = "Lista de TESTE inserida no banco com sucesso!";
	     }
	     return retorno;
	    }
	
}
