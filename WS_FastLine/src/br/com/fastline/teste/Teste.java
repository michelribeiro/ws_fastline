package br.com.fastline.teste;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Teste {
	private Integer id_teste;

	public Integer getId_teste() {
		return id_teste;
	}

	public void setId_teste(Integer id_teste) {
		this.id_teste = id_teste;
	}

	public Teste(Integer id_teste) {
		super();
		this.id_teste = id_teste;
	}

	public Teste() {
		
	}

	@Override
	public String toString() {
		return "Teste [id_teste=" + id_teste + "]";
	}
//
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result
//				+ ((id_teste == null) ? 0 : id_teste.hashCode());
//		return result;
//	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Teste other = (Teste) obj;
		if (id_teste == null) {
			if (other.id_teste != null)
				return false;
		} else if (!id_teste.equals(other.id_teste))
			return false;
		return true;
	}
	
	
}
