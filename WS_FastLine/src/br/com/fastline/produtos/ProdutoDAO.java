package br.com.fastline.produtos;

import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import br.com.fastline.factory.ConnectionFactory;

public class ProdutoDAO extends ConnectionFactory {

	public Date formataData(Date data) {
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		String str = formatador.format(data.getTime());
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
		Date format = null;
		try {
			format = (java.sql.Date)formatter.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}  
		return format;
	}

	public ArrayList<Produto> listarTodos(){

		Connection conn = null;
		ResultSet resultSet = null;
		PreparedStatement stmt = null;
		ArrayList<Produto> produtos = null;

		conn = getConnection();
		produtos = new ArrayList<Produto>();

		try {

			stmt = conn.prepareStatement("SELECT * FROM tb_cad_produtos");
			resultSet = stmt.executeQuery();

			while (resultSet.next()) {
				Produto produto = new Produto();
				produto.setId_produto(resultSet.getInt("id_produto"));
				produto.setNm_produto(resultSet.getString("nm_produto"));
				produto.setCusto(resultSet.getFloat("custo"));
				produto.setPreco_venda(resultSet.getFloat("preco_venda"));
				//	             produto.setDt_cadastro(formataData((resultSet.getDate("dt_cadastro"))));
				//	             produto.setDt_inativacao(formataData((resultSet.getDate("dt_inativacao"))));
				produto.setDt_cadastro(resultSet.getDate("dt_cadastro"));
				produto.setDt_inativacao(resultSet.getDate("dt_inativacao"));
				produto.setSaldo_estoque(resultSet.getInt("saldo_estoque"));
				produto.setStatus(resultSet.getInt("status"));

				produtos.add(produto);
			}

		} catch (SQLException e) {
			System.out.println("Erro ao listar todos os clientes: " + e);
			e.printStackTrace();
			//	         e.printStackTrace();
			//	         listaProdutos = null;
		} finally {
			closeConnection(conn, stmt, resultSet);
		}
		return produtos;
	}
	
//	public String inserir() {
//
//	     Connection conn = null;
//	     ResultSet resultSet = null;
//	     PreparedStatement stmt = null;
//	     String resultado = null;
//	     conn = getConnection();
//	     try {
//	         stmt = conn.prepareStatement("SELECT fn_crud_produtos(NULL,'pipoca',10.00,20.00,10,1,'INSERT')");
////	         stmt.setString(1, produto.getNm_produto());
////	         stmt.setFloat(2, produto.getCusto());
////	         stmt.setFloat(3, produto.getPreco_venda());
////	         stmt.setInt(4, produto.getSaldo_estoque());
////	         stmt.setInt(5, produto.getStatus());
//
//	         resultSet = stmt.executeQuery();
//	         while (resultSet.next()) {
//	        	 resultado = resultSet.getString(1);
//	         }
//	     } catch (SQLException e) {
//	         e.printStackTrace();
//	     } finally {
//	         closeConnection(conn, stmt, resultSet);
//	     }
//	     return resultado;
//	}

	
	
	public int inserir(Produto produto) {

	     Connection conn = null;
	     conn = getConnection();
	     int sucesso = 0;

	         PreparedStatement stmt = null;
	         try {
	             stmt = conn.prepareStatement("INSERT INTO tb_cad_produtos (id_produto,nm_produto,custo,preco_venda,dt_cadastro,dt_inativacao,saldo_estoque,status) VALUES(?,?,?,?,?,?,?,?)");
	             stmt.setInt(1, produto.getId_produto());
	             stmt.setString(2, produto.getNm_produto());
	             stmt.setFloat(3, produto.getCusto());
	             stmt.setFloat(4, produto.getPreco_venda());
	             stmt.setDate(5, (java.sql.Date) produto.getDt_cadastro());
	             stmt.setDate(6, (java.sql.Date) produto.getDt_inativacao());
	             stmt.setInt(7, produto.getSaldo_estoque());
	             stmt.setInt(8, produto.getStatus());
	             
	             sucesso = stmt.executeUpdate();

	             if (sucesso > 0) {
	                 System.out.println("CLIENTE INSERIDO!");
	             }

	         } catch (SQLException e) {
	             e.printStackTrace();
	             System.out.println("ERRO AO INSERIR CLIENTE!");
	         } finally {
	             closeConnection(conn, stmt);
	         }
	     return sucesso;
	    }
	
}
