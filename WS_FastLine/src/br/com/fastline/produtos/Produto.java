package br.com.fastline.produtos;






import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public final class Produto {
	
	private Integer id_produto;
	private String nm_produto;
	private Float custo;
	private Float preco_venda;
	private Date dt_cadastro;
	private Date dt_inativacao;
	private Integer saldo_estoque;
	private Integer status;
	
	public Integer getId_produto() {
		return id_produto;
	}
	public void setId_produto(Integer id_produto) {
		this.id_produto = id_produto;
	}
	public String getNm_produto() {
		return nm_produto;
	}
	public void setNm_produto(String nm_produto) {
		this.nm_produto = nm_produto;
	}
	public Float getCusto() {
		return custo;
	}
	public void setCusto(Float custo) {
		this.custo = custo;
	}
	public Float getPreco_venda() {
		return preco_venda;
	}
	public void setPreco_venda(Float preco_venda) {
		this.preco_venda = preco_venda;
	}
	public Date getDt_cadastro() {
		return dt_cadastro;
	}
	public void setDt_cadastro(Date dt_cadastro) {
		this.dt_cadastro = dt_cadastro;
	}
	public Date getDt_inativacao() {
		return dt_inativacao;
	}
	public void setDt_inativacao(Date dt_inativacao) {
		this.dt_inativacao = dt_inativacao;
	}
	public Integer getSaldo_estoque() {
		return saldo_estoque;
	}
	public void setSaldo_estoque(Integer saldo_estoque) {
		this.saldo_estoque = saldo_estoque;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Produto [id_produto=" + id_produto + ", nm_produto="
				+ nm_produto + ", custo=" + custo + ", preco_venda="
				+ preco_venda + ", dt_cadastro=" + dt_cadastro
				+ ", dt_inativacao=" + dt_inativacao + ", saldo_estoque="
				+ saldo_estoque + ", status=" + status + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((custo == null) ? 0 : custo.hashCode());
		result = prime * result
				+ ((dt_cadastro == null) ? 0 : dt_cadastro.hashCode());
		result = prime * result
				+ ((dt_inativacao == null) ? 0 : dt_inativacao.hashCode());
		result = prime * result
				+ ((id_produto == null) ? 0 : id_produto.hashCode());
		result = prime * result
				+ ((nm_produto == null) ? 0 : nm_produto.hashCode());
		result = prime * result
				+ ((preco_venda == null) ? 0 : preco_venda.hashCode());
		result = prime * result
				+ ((saldo_estoque == null) ? 0 : saldo_estoque.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (custo == null) {
			if (other.custo != null)
				return false;
		} else if (!custo.equals(other.custo))
			return false;
		if (dt_cadastro == null) {
			if (other.dt_cadastro != null)
				return false;
		} else if (!dt_cadastro.equals(other.dt_cadastro))
			return false;
		if (dt_inativacao == null) {
			if (other.dt_inativacao != null)
				return false;
		} else if (!dt_inativacao.equals(other.dt_inativacao))
			return false;
		if (id_produto == null) {
			if (other.id_produto != null)
				return false;
		} else if (!id_produto.equals(other.id_produto))
			return false;
		if (nm_produto == null) {
			if (other.nm_produto != null)
				return false;
		} else if (!nm_produto.equals(other.nm_produto))
			return false;
		if (preco_venda == null) {
			if (other.preco_venda != null)
				return false;
		} else if (!preco_venda.equals(other.preco_venda))
			return false;
		if (saldo_estoque == null) {
			if (other.saldo_estoque != null)
				return false;
		} else if (!saldo_estoque.equals(other.saldo_estoque))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	
		
}
